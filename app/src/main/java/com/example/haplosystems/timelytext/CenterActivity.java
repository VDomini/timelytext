package com.example.haplosystems.timelytext;

import android.app.ActionBar;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.content.ContentResolver;
import android.database.Cursor;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CenterActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private static MessagesDataSource datasource;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_center);

        Toolbar aTB = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(aTB);
        aTB.setLogo(R.mipmap.night_owl);

        datasource = new MessagesDataSource(this);
        datasource.open();

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(1, false);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_center, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            Button e = (Button)getActivity().findViewById(R.id.dbut);
            String dText = (month + 1) + "/" + day + "/" + year;
            e.setText(dText);
        }
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of DatePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute, android.text.format.DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hour, int minute) {
            // Do something with the date chosen by the user
            Button e = (Button)getActivity().findViewById(R.id.tbut);
            String t = "";
            if(minute != 0) {
                t = hour + ":" + minute;
            }
            else
            {
                t = hour + ":00";
            }
            e.setText(t);
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }


        private ArrayAdapter<String> adapter;

        // Store contacts values in these arraylist
        public static ArrayList<String> phoneValueArr = new ArrayList<String>();
        public static ArrayList<String> nameValueArr = new ArrayList<String>();

        private void readContactData() {

            try {

                /*********** Reading Contacts Name And Number **********/

                String phoneNumber = "";
                ContentResolver cr = getActivity().getBaseContext()
                        .getContentResolver();

                //Query to get contact name

                Cursor cur = cr
                        .query(ContactsContract.Contacts.CONTENT_URI,
                                null,
                                null,
                                null,
                                null);

                // If data data found in contacts
                if (cur.getCount() > 0) {


                    int k=0;
                    String name = "";

                    while (cur.moveToNext())
                    {

                        String id = cur
                                .getString(cur
                                        .getColumnIndex(ContactsContract.Contacts._ID));
                        name = cur
                                .getString(cur
                                        .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                        //Check contact have phone number
                        if (Integer
                                .parseInt(cur
                                        .getString(cur
                                                .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
                        {

                            //Create query to get phone number by contact id
                            Cursor pCur = cr
                                    .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                            null,
                                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                                    + " = ?",
                                            new String[] { id },
                                            null);
                            int j=0;

                            while (pCur
                                    .moveToNext())
                            {
                                // Sometimes get multiple data
                                if(j==0)
                                {
                                    // Get Phone number
                                    phoneNumber =""+pCur.getString(pCur
                                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                                    // Add contacts names to adapter
                                    adapter.add(name + " : " + phoneNumber);

                                    // Add ArrayList names to adapter
                                    phoneValueArr.add(phoneNumber.toString());
                                    nameValueArr.add(name.toString() + " : " + phoneNumber.toString());

                                    j++;
                                    k++;
                                }
                            }  // End while loop
                            pCur.close();
                        } // End if

                    }  // End while loop

                } // End Cursor value check
                cur.close();


            } catch (Exception e) {
                Log.i("AutocompleteContacts", "Exception : " + e);
            }


        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            //Here is where we differentiate between tabs.
            int tabno = getArguments().getInt(ARG_SECTION_NUMBER);
            View rootView;

            if( tabno == 1)
            {
                //Left Tab. This will be Pending Messages
                //Fairly simple, load messages that are not sent from database
                //Include send time, message content, recipient. Possibility to delete them.
                rootView = inflater.inflate(R.layout.fragment_left, container, false);
                List<TimelyMessage> mes = datasource.getAllPendingMessages();

                TimelyAdapter adap = new TimelyAdapter(getActivity(), R.layout.custom_listview_row, mes);
                ListView l1 = (ListView)rootView.findViewById(R.id.lview2);

                //View header = (View)inflater.inflate(R.layout.custom_listview_row, null);
                //l1.addHeaderView(header);
                l1.setAdapter(adap);




            }
            else if(tabno == 2)
            {
                //Center Tab. This will be the New Message Screen
                //Selector for recipient and time. Message box for message.
                //Button for send, button for clear.
                rootView = inflater.inflate(R.layout.fragment_center, container, false);
                final AutoCompleteTextView tview = (AutoCompleteTextView) rootView.findViewById(R.id.tview);

                //Create adapter
                adapter = new ArrayAdapter<String>
                        (getActivity(), android.R.layout.simple_dropdown_item_1line, new ArrayList<String>());
                tview.setThreshold(1);

                //Set adapter to AutoCompleteTextView
                tview.setAdapter(adapter);
                /*tview.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        int index = nameValueArr.indexOf(tview.getText().toString());
                        String item = phoneValueArr.get(index);
                        t.setText(item);

                    }
                });*/
                final Button date = (Button) rootView.findViewById(R.id.dbut);
                final Button time = (Button) rootView.findViewById(R.id.tbut);
                final EditText message = (EditText) rootView.findViewById(R.id.message);
                final CheckBox recur = (CheckBox) rootView.findViewById(R.id.cBox);
                Button create = (Button) rootView.findViewById(R.id.create);
                Button clear = (Button) rootView.findViewById(R.id.clear);

                create.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if( nameValueArr.contains( tview.getText().toString()) && !date.getText().toString().equals("")
                                && !time.getText().toString().equals("") && !message.getText().toString().equals("")){
                            String pNum = tview.getText().toString();
                            String d = date.getText().toString() + " " + time.getText().toString();
                            String mes = message.getText().toString();
                            boolean isR = recur.isChecked();
                            Date fdate = new Date();

                            DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm");
                            try {
                                fdate = df.parse(d);
                            }
                            catch(Exception e) {
                                e.printStackTrace();
                            }

                            datasource.createMessage(mes, pNum, fdate, isR, true);
                            Snackbar.make(v, "Message Created!", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();

                            date.setText("Select Date");
                            message.setText("");
                            recur.setChecked(false);
                            time.setText("Select Time");
                            tview.setText("");

                        }
                        else{
                            Snackbar.make(v, "You are missing or have some invalid information!", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                    }
                });

                clear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        date.setText("Select Date");
                        message.setText("");
                        recur.setChecked(false);
                        time.setText("Select Time");
                        tview.setText("");
                    }
                });


                // Read contact data and add data to ArrayAdapter
                // ArrayAdapter used by AutoCompleteTextView

                readContactData();

            }
            else
            {
                //Right Tab. Message History
                //Load sent from database. Option to clear history.
                rootView = inflater.inflate(R.layout.fragment_right, container, false);

                List<TimelyMessage> mes = datasource.getAllSentMessages();

                TimelyAdapter adap = new TimelyAdapter(getActivity(), R.layout.custom_listview_row, mes);
                ListView l1 = (ListView)rootView.findViewById(R.id.lview);

                //View header = (View)inflater.inflate(R.layout.custom_listview_row, null);
                //l1.addHeaderView(header);
                l1.setAdapter(adap);

            }
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "PENDING";
                case 1:
                    return "NEW MESSAGE";
                case 2:
                    return "SENT";
            }
            return null;
        }
    }

    public static class SchedulerSetupReceiver extends WakefulBroadcastReceiver {
        private static final String APP_TAG = "timelytext.scheduler";

        private static final int EXEC_INTERVAL = 60 * 1000;

        @Override
        public void onReceive(final Context ctx, final Intent intent) {
            Log.d(APP_TAG, "SchedulerSetupReceiver.onReceive() called");
            AlarmManager alarmManager = (AlarmManager) ctx
                    .getSystemService(Context.ALARM_SERVICE);
            Intent i = new Intent(ctx, SchedulerEventReceiver.class); // explicit
            // intent
            PendingIntent intentExecuted = PendingIntent.getBroadcast(ctx, 0, i,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            Calendar now = Calendar.getInstance();
            now.add(Calendar.SECOND, 60);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                    now.getTimeInMillis(), EXEC_INTERVAL, intentExecuted);
        }

    }

    public static class SchedulerEventService extends Service {
        private static final String APP_TAG = "timelytext.scheduler";

        @Override
        public IBinder onBind(final Intent intent) {
            return null;
        }

        @Override
        public int onStartCommand(final Intent intent, final int flags,
                                  final int startId) {
            Log.d(APP_TAG, "event received in service: " + new Date().toString());

            MessagesDataSource dsource = new MessagesDataSource(this);
            dsource.open();
            dsource.sendPendingMessages();
            dsource.close();
            return Service.START_NOT_STICKY;
        }

    }

    public static class SchedulerEventReceiver extends BroadcastReceiver {
        private static final String APP_TAG = "timelytext";

        @Override
        public void onReceive(final Context ctx, final Intent intent) {
            Log.d(APP_TAG, "SchedulerEventReceiver.onReceive() called");
            Intent eventService = new Intent(ctx, SchedulerEventService.class);
            ctx.startService(eventService);
        }

    }




}
