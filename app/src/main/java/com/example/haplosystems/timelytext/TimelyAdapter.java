package com.example.haplosystems.timelytext;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.view.View;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Rod Aluise on 5/11/2016.
 */
public class TimelyAdapter extends ArrayAdapter<TimelyMessage> {

    Context context;
    int layoutResourceID;
    List<TimelyMessage> data = null;
    DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");

    public TimelyAdapter(Context context, int layoutResourceId, List<TimelyMessage> data)
    {
        super(context, layoutResourceId, data);
        this.layoutResourceID = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View row = convertView;
        MessageHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceID, parent, false);

            holder = new MessageHolder();
            holder.txtTitle1 = (TextView)row.findViewById(R.id.Recip);
            holder.txtTitle2 = (TextView)row.findViewById(R.id.Date);
            holder.txtTitle3 = (TextView)row.findViewById(R.id.Preview);

            row.setTag(holder);
        }
        else
        {
            holder = (MessageHolder)row.getTag();
        }

        TimelyMessage message = data.get(position);
        holder.txtTitle1.setText(message.getRecipient());
        holder.txtTitle2.setText(df.format(message.getTime()));
        holder.txtTitle3.setText(message.getMessagePreview());


        return row;
    }

    static class MessageHolder
    {
        TextView txtTitle1;
        TextView txtTitle2;
        TextView txtTitle3;
    }
}
