package com.example.haplosystems.timelytext;

import java.util.Date;


public class TimelyMessage {
    private String message;
    private String recipient;
    private boolean isRecurring;
    private Date datestamp;
    private long id;

    public TimelyMessage(String m, String r, boolean i, Date d)
    {
        this.message = m;
        this.recipient = r;
        this.isRecurring = i;
        this.datestamp = d;
    }

    public long getID() {
        return id;
    }

    public void setID(long s) {
        this.id = s;
    }

    public String getMessage() {
        return message;
    }

    public String getMessagePreview(){
        if (this.message.length() > 40)
        {
            return (this.message.substring(0,38) + "...");
        }
        else {
            return this.message;
        }
    }

    public void setMessage(String s) {
        this.message = s;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String s) {
        this.recipient = s;
    }

    public boolean getRecurring() {
        return isRecurring;
    }

    public void setRecurring(boolean s) {
        this.isRecurring = s;
    }

    public Date getTime() {
        return datestamp;
    }

    public void setTime(Date s) {
        this.datestamp = s;
    }


}
