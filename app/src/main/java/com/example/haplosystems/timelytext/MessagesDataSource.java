package com.example.haplosystems.timelytext;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.telephony.SmsManager;
import android.util.Log;

public class MessagesDataSource {

    // Database fields
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_MESSAGE, MySQLiteHelper.COLUMN_RECIP,
            MySQLiteHelper.COLUMN_DATE, MySQLiteHelper.COLUMN_BOOL, MySQLiteHelper.COLUMN_PENDING };

    public MessagesDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public TimelyMessage createMessage(String Message, String Recip, Date d, boolean isR, boolean isP) {
        ContentValues values = new ContentValues();
        int iR = isR ? 1 : 0;
        int iP = isP ? 1 : 0;
        values.put(MySQLiteHelper.COLUMN_MESSAGE, Message);
        values.put(MySQLiteHelper.COLUMN_RECIP, Recip);
        values.put(MySQLiteHelper.COLUMN_DATE, d.getTime());
        values.put(MySQLiteHelper.COLUMN_BOOL, iR);
        values.put(MySQLiteHelper.COLUMN_PENDING, iP);


        long insertId = database.insert(MySQLiteHelper.TABLE_MESSAGES, null,
                values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_MESSAGES,
                allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        TimelyMessage newMessage = cursorToMessage(cursor);
        cursor.close();
        return newMessage;
    }

    public void deleteMessage(TimelyMessage Message) {
        long id = Message.getID();
        database.delete(MySQLiteHelper.TABLE_MESSAGES, MySQLiteHelper.COLUMN_ID
                + " = " + id, null);
    }

    public List<TimelyMessage> getAllPendingMessages() {
        List<TimelyMessage> Messages = new ArrayList<TimelyMessage>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_MESSAGES,
                allColumns, MySQLiteHelper.COLUMN_PENDING + "=1", null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            TimelyMessage Message = cursorToMessage(cursor);
            Messages.add(Message);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return Messages;
    }

    public void sendPendingMessages() {

        Cursor cursor = database.query(MySQLiteHelper.TABLE_MESSAGES,
                allColumns, MySQLiteHelper.COLUMN_PENDING + "=1", null, null, null, null);

        Log.d("Is this ever fired", "maybe?");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            TimelyMessage Message = cursorToMessage(cursor);
            if( Message.getTime().getTime() < (new Date()).getTime())
            {
                deleteMessage(Message);

                if( Message.getRecurring()){
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(Message.getTime());
                    cal.add(Calendar.DATE, 1); //minus number would decrement the days
                    Date tii = cal.getTime();

                    createMessage(Message.getMessage(), Message.getRecipient(), Message.getTime(), Message.getRecurring(), false);
                    createMessage(Message.getMessage(), Message.getRecipient(), tii, Message.getRecurring(), true);
                }
                else {
                    createMessage(Message.getMessage(), Message.getRecipient(), Message.getTime(), Message.getRecurring(), false);
                }
                String[] pNumArr = Message.getRecipient().split(" : ");
                String pNum = pNumArr[pNumArr.length - 1];

                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(pNum, null, Message.getMessage(), null, null);
            }
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

    }

    public List<TimelyMessage> getAllSentMessages() {
        List<TimelyMessage> Messages = new ArrayList<TimelyMessage>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_MESSAGES,
                allColumns, MySQLiteHelper.COLUMN_PENDING + "=0", null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            TimelyMessage Message = cursorToMessage(cursor);
            Messages.add(Message);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return Messages;
    }

    private TimelyMessage cursorToMessage(Cursor cursor) {
        String m = cursor.getString(1);
        String r = cursor.getString(2);
        Date d = new Date(cursor.getLong(3));
        boolean i = (cursor.getInt(4) > 0);
        TimelyMessage message = new TimelyMessage(m, r, i, d);
        message.setID(cursor.getInt(0));
        return message;
    }
}

